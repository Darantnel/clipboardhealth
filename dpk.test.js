const { deterministicPartitionKey } = require("./dpk");


describe("deterministicPartitionKey", () => {
  it("Returns the literal '0' when given no input", () => {
    const trivialKey = deterministicPartitionKey();
    expect(trivialKey).toBe("0");
  });
});

describe("deterministicPartitionKey", () => {
  it("Returns hash when given some string input", () => {
    const trivialKey = deterministicPartitionKey('string');
    expect(trivialKey).toBe("f782b910cdf388931df9f00826559deee4b9348dc447cc20b585651b1f5a02203836101a349a150642cb3f9d91ea5c40bf9ab2442caf269db552daa251107562");
  });
});

describe("deterministicPartitionKey", () => {
  it("Returns hash when given some integer input", () => {
    const trivialKey = deterministicPartitionKey(1);
    expect(trivialKey).toBe("ca2c70bc13298c5109ee0cb342d014906e6365249005fd4beee6f01aee44edb531231e98b50bf6810de6cf687882b09320fdd5f6375d1f2debd966fbf8d03efa");
  });
});

describe("deterministicPartitionKey", () => {
  it("Returns '0' when given a null input", () => {
    const trivialKey = deterministicPartitionKey(null);
    expect(trivialKey).toBe("0");
  });
});

describe("deterministicPartitionKey", () => {
  it("Returns hash when given boolean true input", () => {
    const trivialKey = deterministicPartitionKey(true);
    expect(trivialKey).toBe("ff2c82ed266dc30b1afe862bee32cf996b213513bc6b3e242ff605ddd9d5bbd1e7eebf6dde586b8700125cb7b95d35aec2f4e750d092cd359b202e3d2be41e1a");
  });
});

describe("deterministicPartitionKey", () => {
  it("Returns '0' when given boolean false input", () => {
    const trivialKey = deterministicPartitionKey(false);
    expect(trivialKey).toBe("0");
  });
});

let person = {
  partitionKey: 'John'
};

describe("deterministicPartitionKey", () => {
  it("Returns hash when given boolean true input", () => {
    const trivialKey = deterministicPartitionKey(person);
    expect(trivialKey).toBe("John");
  });
});

let person2 = {
  partitionKey: 'wkfjwefgjbwekjfbwejfbwefbwehbfewhjfbwefblwehjfbwjhbfjwhfvbwejhfvbwejhfvbwehjvbfwelhvbfwjlehfvbwjklehvfwehjfvwhfvblsehfgblwehgvbjoehfgbwelhfgbweohfberljfhbwljfhbwejlghbwerlghwerblghweblvhweblwerhblerblwerhblerblwerhgblwehgbjolwerhgbwerjhgbwerjkhfbwejfhbwejlfhbwelfhbweljfhbwelfhbwelfjhbweflhwebflwebfjehblwebfjfwejfblwekfbwlefhjbwejfhbewi'
};

describe("deterministicPartitionKey", () => {
  it("Returns hash when given boolean true input", () => {
    const trivialKey = deterministicPartitionKey(person2);
    expect(trivialKey).toBe("a1791ccda3410a7450660b259ab0672e74fc194dcc924b6e3ea83c354029b37605a6c1b4cfefc2a510857e32f2a58e2171844706fd3ca3ef6a7fc4cea66f0e24");
  });
});