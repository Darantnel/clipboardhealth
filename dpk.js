const crypto = require("crypto");

exports.deterministicPartitionKey = (event) => {
  const TRIVIAL_PARTITION_KEY = "0";
  const MAX_PARTITION_KEY_LENGTH = 256;
  let candidate;

  if (event) {
    if (event.partitionKey) {
      candidate = event.partitionKey;
    } else {
      candidate = crypto.createHash("sha3-512").update(JSON.stringify(event)).digest("hex");
    }
  }

  if (candidate) {
    if (typeof candidate !== "string") {
        candidate = JSON.stringify(candidate);
    }
    if (candidate.length > MAX_PARTITION_KEY_LENGTH) {
      candidate = crypto.createHash("sha3-512").update(candidate).digest("hex");
    }
    return candidate;
  } else {
    return TRIVIAL_PARTITION_KEY;
  }  
};

// Code Refactor Paragraph: The first change made was to directly stringify the event 
//variable into the has function, it removes an irellavant creation of the 'data' variable , 
//readers of code will simply see the action done to the event and it's usage as a parameter 
//to the createHash function.



// The second change made was the movement of the conditions for the candidate variables and return actions.
// The final if statement will never pass if the candidate is set to the TRIVIAL_PARTITON_KEY so we simply 
//added a return statement in place of the assignment of that constant to the candidate variable. 
//Readers of the code will know once a candidate value doesn't exist the code returns a zero value. 
//Similarly a return was places after the stringify and length check conditions of this function. 
//Code readers will know whatever candidate values entered the first candidate condition will be returned 
//whether it has passed or failed the type and length checks. 
